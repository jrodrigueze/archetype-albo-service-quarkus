# Maven Archetype for Quarkus Service

This projects defines a maven archetype for bootstraping a Quarkus Service using **albo** practices

## Usage

In order to install the archetype in local repo, run the following mvn command:


```sh
mvn clean install
```


In order to generate a new project using this archetype, use the following mvn commands:

### Interactive mode
Interactive mode allows to specify various properties used at generation time, if defaults should not be used.


```sh
mvn archetype:generate \
 -DarchetypeGroupId=mx.albo.archetype \
 -DarchetypeArtifactId=archetype-albo-service-quarkus \
 -DarchetypeVersion=1.0-SNAPSHOT
```


### Non-interactive mode
Interactive mode can be bypassed by passing all needed properties


```sh
mvn archetype:generate \
 -DarchetypeGroupId=mx.albo.archetype \
 -DarchetypeArtifactId=archetype-albo-service-quarkus \
 -DarchetypeVersion=1.0-SNAPSHOT \
 -DgroupId=<GroupId> \
 -Dalbo-service-domain=<ServiceDomain> \
 -Dalbo-service-cluster=<ServiceCluster>
 -Dalbo-data-version=<DataLibVersion> \
 -Dalbo-vault-config-version=<VaultLibVersion> \
 -Dalbo-service-version=<ServiceLibVersion> \
 -Dalbo-utils-version=<UtilsLibVersion> \
 -Dservice-package-name=<ServicePackage> \
 -DinteractiveMode=false
```



where:

| Property                      | Usage     | Description                                                                     | Default value                                       |   |
|-------------------------------|-----------|---------------------------------------------------------------------------------|-----------------------------------------------------|---|
| __archetypeGroupId__          | MANDATORY | Group Id of the archetype to be used.                                           | mx.albo.archetype                                   |   |
| __archetypeArtifactId__       | MANDATORY | Artifact Id of the archetype to be used.                                        | archetype-albo-service-quarkus                      |   |
| __archetypeVersion__          | MANDATORY | Version of this archetype that should be used. Use accordingly.                 |                                                     |   |
| __groupId__                   | OPTIONAL  | Group Id of the project to be generated.                                        | mx.albo.mirai.service                               |   |
| __artifactId__                | MANDATORY | Artifact Id of the project to be generated.                                     |                                                     |   |
| __albo-service-domain__       | MANDATORY | Domain the service belongs. Used for pipelines.                                 |                                                     |   |
| __albo-service-cluster__      | OPTIONAL  | Cluster the service belongs. Used for pipelines.                                | mirai-cluster-dev                                   |   |
| __albo-data-version__         | OPTIONAL  | Version of albo data library.                                                   |                                                     |   |
| __albo-vault-config-version__ | OPTIONAL  | Version of albo vault config library.                                           |                                                     |   |
| __albo-service-version__      | OPTIONAL  | Version of albo service library.                                                |                                                     |   |
| __albo-utils-version__        | OPTIONAL  | Version of albo utils library.                                                  |                                                     |   |
| __service-package-name__      | OPTIONAL  | Name of the subpackage specific for the component, under the groupId hierarchy. | Defaults to artifactId, replacing "-" with "_".     |   |
| __interactiveMode__           | OPTIONAL  | If false, no prompt for additional values and will use default.                 | true                                                |   |