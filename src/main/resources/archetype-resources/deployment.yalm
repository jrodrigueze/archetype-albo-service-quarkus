apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: $SERVICE_NAME
  name: $SERVICE_NAME
spec:
  replicas: $REPLICAS
  selector:
    matchLabels:
      app: $SERVICE_NAME
      tier: web
  strategy:
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: $SERVICE_NAME
        tier: web
    spec:
      hostAliases:
      - ip: "127.0.0.1"
        hostnames:
        - "$SERVICE_NAME.local"
      - ip: "::1"
        hostnames:
        - "$SERVICE_NAME.local"
      containers:
      - env:
        - name: VAULT_TOKEN
          value: $VAULT_TOKEN_DEPLOY
        - name: VAULT_ADDR
          value: $VAULT_ADDR_DEPLOY
        image: gcr.io/$PROJECT_ID/albo/mirai/$DOMAIN/$IMAGE:$TAG
        name: $IMAGE
        ports:
        - containerPort: 8080
        readinessProbe:
          failureThreshold: 1
          httpGet:
            path: /health/ready
            port: 8080
          initialDelaySeconds: 30
          periodSeconds: 10
          timeoutSeconds: 3
        resources:
          limits:
            cpu: $L_CPU
            memory: $L_MEM
          requests:
            cpu: $R_CPU
            memory: $R_MEM
        volumeMounts:
        - mountPath: "/opt/secret"
          name: secret
          readOnly: true
      volumes:
        - name: secret
          secret:
            secretName: secrets-reader
            items:
              - key: service_account
                path: my-group/service-account
