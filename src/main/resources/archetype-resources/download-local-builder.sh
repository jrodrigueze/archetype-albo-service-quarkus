#!/usr/bin/env bash
gsutil cp gs://mirai-tools/local-builder/1.0/local-build .
chmod +x local-build
echo "Ambientar proyecto:"
echo "   ./local-build --first-step"
echo "Compilar local:"
echo "   ./local-build --mavenpath=../../albo-mx-apps-mavenreader.json --env=dev --test=false"
