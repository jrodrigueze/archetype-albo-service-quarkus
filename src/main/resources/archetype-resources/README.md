#set($h1='#')
#set($h2='##')
#set($h3='###')
#set($h4='####')

${h1} Service ${artifactId}

__Service name__: ${artifactId}

__Service domain__: ${albo-service-domain}

${h2} Service description

[Include a brief description of the service]

${h2} Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

Alternatively, libs version may be passed when running:

```shell
mvn compile quarkus:dev \
 -Denv.MAVEN_REPOSITORY=albo-mirai-repository-release \
 -Denv.SERVICE_VERSION=[version] \
 -Denv.DATA_VERSION=[version] \
 -Denv.EVENT_VERSION=[version] \
 -Denv.UTILS_VERSION=[version] \
 -Denv.VAULT_CONFIG_VERSION=[version]
 ```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

${h2} Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

${h2} Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/${artifactId}-${version}-runner`

\
&nbsp;
\
&nbsp;
 

--------------------------------------------------------------------
> **_Please delete this from this section on before committing. It is only intended for understanding the structure of this service._**

\
&nbsp;


${h1} Understanding this archetype


${h2} Packages


- Endpoint. Includes classes that expose a Rest endpoint.
- Service. Includes service classes, which contain business logic.
- Repository. Includes classes that access databases.
- Util. Includes various utility classes.


${h2} Java classes


${h3} Controller.java


Entrypoint for Rest call. This class includes a default Post method with an endpoint. It should always return an `albo.mirai.utils.response.AlboResponse.ok` or `albo.mirai.utils.response.AlboResponse.bad` response.

albo endpoints use the following naming convention:

```plain
/{service-domain}/{entity}/{verb}@1.0
```

- service-domain. The domain the service belongs. This parameter is passed when building a project from archetype. Examples include "coupon", "space_account", etc.
- entity. The noun upon which the service provides functionality.
- verb. A command that describes the purpose of the service.

Examples.

1. A service from "coupons" domain that validates their expiration date.
```plain
/coupons/expiration/validate@1.0
```

2. A service from "customer" domain that counts customers grouped by city
```plain
/customer/city/count@1.0
```

Furthermore, Post methods should be documented with available tags to generate Swagger UI documentation, which must be uploaded to the [Confluence repository](https://albomx.atlassian.net/wiki/home) before release.

An example documentation would be:

```java
    @POST
    @Path("/describe@1.0")
    @Tag(name = "Describe@1.0")
    @APIResponses(value = {
            @APIResponse(responseCode = RestData.CODE_200, description = RestData.DESCRIPTION, content = @Content(mediaType = RestData.CONTENT_TYPE, example = RestData.EXAMPLE)),
            @APIResponse(responseCode = RestData.CODE_400, description = RestData.DESCRIPTION_400, content = @Content(mediaType = RestData.CONTENT_TYPE, example = RestData.EXAMPLE_400)),
            @APIResponse(responseCode = RestData.CODE_200_A, description = RestData.DESCRIPTION_A, content = @Content(mediaType = MediaType.APPLICATION_JSON, example = RestData.EXAMPLE_A)),
            @APIResponse(responseCode = RestData.CODE_200_B, description = RestData.DESCRIPTION_B, content = @Content(mediaType = MediaType.APPLICATION_JSON, example = RestData.EXAMPLE_B)),
            @APIResponse(responseCode = RestData.CODE_200_C, description = RestData.DESCRIPTION_C, content = @Content(mediaType = MediaType.APPLICATION_JSON, example = RestData.EXAMPLE_C)),
            @APIResponse(responseCode = RestData.CODE_200_D, description = RestData.DESCRIPTION_D, content = @Content(mediaType = MediaType.APPLICATION_JSON, example = RestData.EXAMPLE_D))
    })
    @Operation(summary = RestData.SUMMARY, description = RestData.GENERAL_DESCRIPTION)
    @RequestBody(content = @Content(mediaType = RestData.CONTENT_TYPE, schema = @Schema(type = SchemaType.OBJECT), example = RestData.REQUEST))
    @Produces(MediaType.APPLICATION_JSON)
    public Object service(String payload) {
		// method implementation
	}
```

The Controller class includes a default Scheduled method, which should be kept in order to optimize DB connectivity.


${h3} Service.java


Includes the main business logic and uses repositories, utility classes and other services to fulfill requests.


${h3} Repository


Includes methods for accessing database. 

It is recommended, when feasible, to use methods from DataBaseHelper class, in **data** project (https://bitbucket.org/albomirai/data), branch `develop`.


${h2} Test classes

The service includes the following test classes, using JUnit5 + Mockito, enabled out-of-the-box by Quarkus.

${h3} ControllerTest.java


Tests the whole endpoint call


${h3} ServiceTest.java


Can test various methods from the service.


${h3} MockRepository.java


Used to bypass calls to database and mock a response. Direct calls to databases in tests should be avoided.

> **_NOTE:_**  These default test classes are just samples. Additional classes and methods must be added in order to increase coverage.

${h2} Configuration files


${h3} application.properties

Contains basic configuration.

It may include endpoints to other services, service-wide configuration parameters, database config*, etc.

> **_NOTE:_**  *Database configuration in application.properties are merely used for dev profile and can be safely excluded from commits.

${h3} vault.properties


Contains a list of secrets, generally used in production profile (environments dev, qa, sbx and prod). It contains a list of all available databases. Uncomment accordingly.


${h3} pom.xml


Standard pom file for maven.

By default, it includes the following lines commented, which match the library versions specified (or defaults) when building project from archetype. These are **only needed in certain local execution environments**, where they can't be read as environment properties.

```xml
        <!--
        <env.MAVEN_REPOSITORY>albo-mirai-repository-release</env.MAVEN_REPOSITORY>
        <env.SERVICE_VERSION>{albo-service-version}</env.SERVICE_VERSION>
        <env.DATA_VERSION>{albo-data-version}</env.DATA_VERSION>
        <env.UTILS_VERSION>{albo-utils-version}</env.UTILS_VERSION>
        <env.VAULT_CONFIG_VERSION>{albo-vault-config-version}</env.VAULT_CONFIG_VERSION>
        -->
```
For development, they may be uncommented, in order to pass required parameters. However, they **should never** be commited to repository, since production profile uses properties from _bitbucket-pipelines.yml_ .


${h2} Other config files


Several config files are used for Continous Integration tasks and should be managed and edited only by Delivery Team. These files include:
- bitbucket-pipelines.yml
- deployment.yml
- Dockerfile
- _download-local-builder.sh
- service.yalm