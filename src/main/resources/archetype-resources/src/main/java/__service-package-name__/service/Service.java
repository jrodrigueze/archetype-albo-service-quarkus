package ${package}.${service-package-name}.service;

import ${package}.${service-package-name}.repository.Repository;
import ${package}.${service-package-name}.util.Constants;

import static com.mongodb.client.model.Filters.eq;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONObject;


/**
 * Service class
 * 
 * @author ${username}
 * @version 1.0
 * @since 1.0
 */
@ApplicationScoped
public class Service {

    @Inject
    Repository repository;

    /**
     * This method executes the main logic of the service
     * 
     * @param jsonPayload the payload received from the request
     * @return a JSONObject instance that will be returned inside the "data" node in
     *         in JSON response
     */
    public JSONObject execute(JSONObject jsonPayload) {
        JSONObject data = jsonPayload.getJSONObject(Constants.DATA);
        String myFieldValue = data.getString("foo");

        Bson filter = eq("collection_field_name", myFieldValue);
        Document sort = new Document("cdate", -1);

        List<JSONObject> results = repository.findSomething(filter, sort);
        return formatResult(results);
    }

    /**
     * Methods should be documented using javadoc best practices. For further
     * details, refer to <a href=
     * "https://www.oracle.com/technical-resources/articles/java/javadoc-tool.html">official
     * Javadoc documentation</a>
     * 
     * @param results a list of <code>JSONObject</code> retrieved from DB
     * @return a list of formatted JSONObjects
     */
    public JSONObject formatResult(List<JSONObject> results) {
        // do some complex formatting
        return new JSONObject().put("results", results);
    }
    
}
