package ${package}.${service-package-name}.repository;

import albo.mirai.data.database.DataBaseHelper;
import albo.mirai.data.database.MongoDB;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONObject;

import javax.enterprise.context.ApplicationScoped;

import java.util.List;

/**
 * Repository class that contains the database access methods.
 *
 * @author ${username}
 * @version 1.0
 * @since 1.0
 */
@ApplicationScoped
public class Repository {

    // database and collection values depend on service requirements
    private static final MongoDB.DBEnum DATABASE = MongoDB.DBEnum.CUSTOMER;
    private static final String COLLECTION = "os.customer";

    /**
     * Retrieves data from database, using <code>filters</code>
     * 
     * @param filters mongo db filters used for query
     * @return a list of <code>JSONObject</code> instances
     */
    public List<JSONObject> findSomething(Bson filters, Document sort) {
        // just to demonstrate pagination options
        int pageNumber = 1;
        int resultsPerPage = 10;

        // check all available and overloaded methods in DataBaseHelper class
        // https://bitbucket.org/albomirai/data/
        return DataBaseHelper.findDocumentList(DATABASE, COLLECTION, filters, sort, pageNumber, resultsPerPage,
                false);
    }
}
