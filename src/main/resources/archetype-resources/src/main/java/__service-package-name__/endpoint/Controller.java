package ${package}.${service-package-name}.endpoint;

import ${package}.${service-package-name}.util.RestData;
import ${package}.${service-package-name}.service.Service;
import albo.mirai.utils.logger.LoggerManager;
import albo.mirai.utils.response.AlboResponse;

import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import javax.inject.Inject;
import org.json.JSONObject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Endpoint class.
 *
 * @author ${username}
 * @version 1.0
 * @since 1.0
 */
@Path("/${albo-service-domain}/entity")
public class Controller {

    private static final Logger LOG = Logger.getLogger(Controller.class.getName());

    @Inject
    Service service;

    /**
     * Scheduler that resets db connections to improve performance
     */
    @io.quarkus.scheduler.Scheduled(every = "30m")
    void dbHealthCheck() {
        albo.mirai.data.database.InitDB.initConnections();
    }

    /**
     * Entrypoint for rest controller
     *
     * @param payload Contains the data
     * @return
     */
    @POST
    @Path("/verb@1.0")
    @Tag(name = "Tag@1.0")
    @APIResponses(value = {
            @APIResponse(responseCode = RestData.CODE_200, description = RestData.DESCRIPTION, content = @Content(mediaType = RestData.CONTENT_TYPE, example = RestData.EXAMPLE)),
            @APIResponse(responseCode = RestData.CODE_400, description = RestData.DESCRIPTION_400, content = @Content(mediaType = RestData.CONTENT_TYPE, example = RestData.EXAMPLE_400))

    })
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = RestData.SUMMARY, description = RestData.GENERAL_DESCRIPTION)
    @RequestBody(content = @Content(mediaType = RestData.CONTENT_TYPE, schema = @Schema(type = SchemaType.OBJECT), example = RestData.REQUEST))
    public Object service(String payload) {
        LoggerManager.getInstance().logMessage(LOG, Level.INFO, "Payload => " + payload);
        try {
            // add service implementation
            JSONObject response = service.execute(new JSONObject(payload));
            return AlboResponse.ok(response);
        } catch (Exception e) {
            LoggerManager.getInstance().logMessage(LOG, Level.SEVERE, e);
            return AlboResponse.bad(RestData.CODE_SERVICE + RestData.CODE_400_NT, RestData.DETAIL_400);
        }
    }
}
