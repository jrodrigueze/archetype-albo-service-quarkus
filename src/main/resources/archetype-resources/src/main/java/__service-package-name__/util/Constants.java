package ${package}.${service-package-name}.util;


/**
 * Service wide constants. 
 *
 * @author ${username}
 * @version 1.0
 * @since 1.0
 */
public final class Constants {

    private Constants(){
    }

    public static final String DATA = "data";
}
