package ${package}.${service-package-name}.service;

import io.quarkus.test.junit.QuarkusTest;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class ServiceTest {

    @Inject
    Service service;

    @BeforeEach
    public void setUp(){
        // insert code to perform initialization tasks before each test
    }

    @AfterEach
    public void reset(){
        // insert code to perform cleanup tasks after each test
    }

    @Test
    @DisplayName("Should do something")
    public void shouldDoSomething() {
        // prepare data for test
        List<JSONObject> results = Collections.<JSONObject>emptyList();

        // call actual service
        JSONObject actualValue = service.formatResult(results);

        // assert results
        assertTrue(actualValue.has("results"), "This message shows when assertion fails");

        /*
        There are several assertion methods that can be used. Use accordingly.

        assertEquals(expectedValue, actualValue, "error message");
        assertNotNull(actualValue, "error message");
        assertTimeout(java.time.Duration.ofSeconds(5), () -> { service.execute();}, "error message");
        assertThrows(NullPointerException.class, () -> { service.execute();}, "error message");
        */

    }
}
