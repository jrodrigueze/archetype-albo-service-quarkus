package ${package}.${service-package-name}.endpoint;

import io.quarkus.test.junit.QuarkusTest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class ControllerTest {

    private static final String ENDPOINT = "/${albo-service-domain}/entity/verb@1.0";
    
    @Test
    @DisplayName("Should do something")
    public void shouldDoSomething() {
        String jsonPayload = "{ \"data\":{ \"foo\":\"bar\" } }";
        String expectedContentInBody = "results";
        int expectedStatusCode = 200;

        given().header("Content-Type", "application/json")
        .urlEncodingEnabled(false)
        .request()
        .body(jsonPayload)
        .when().post(ENDPOINT)
        .then().statusCode(is(expectedStatusCode))
        .body(containsString(expectedContentInBody));
    }
}
