package ${package}.${service-package-name}.repository;

import java.util.Collections;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONObject;

import io.quarkus.test.Mock;

@Mock
@ApplicationScoped
public class MockRepository extends Repository {

    @Override
    public List<JSONObject> findSomething(Bson filter, Document sort) {
        return Collections.<JSONObject>emptyList();
    }
}